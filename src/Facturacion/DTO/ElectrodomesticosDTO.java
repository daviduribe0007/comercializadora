package Facturacion.DTO;

public class ElectrodomesticosDTO {

    private String referencia;
    private double precioConsumo;
    private double precioProcedencia;

    public ElectrodomesticosDTO(String referencia, double precioConsumo, double precioProcedencia) {
        this.referencia = referencia;
        this.precioConsumo = precioConsumo;
        this.precioProcedencia = precioProcedencia;
    }

    public String getreferencia() {
        return referencia;
    }

    public void setreferencia(String referencia) {
        this.referencia = referencia;
    }

    public double getPrecioConsumo() {
        return precioConsumo;
    }

    public void setPrecioConsumo(double precioConsumo) {
        this.precioConsumo = precioConsumo;
    }

    public double getPrecioProcedencia() {
        return precioProcedencia;
    }

    public void setPrecioProcedencia(double precioProcedencia) {
        this.precioProcedencia = precioProcedencia;
    }

    public double CalcularTotal() {
        return this.precioProcedencia + this.precioConsumo;
    }

}
