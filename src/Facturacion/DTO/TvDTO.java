package Facturacion.DTO;

public class TvDTO extends ElectrodomesticosDTO {

    private double pulgadas;
    private boolean sinconizadorTDT;

    public TvDTO(String referencia, double precioConsumo, double precioProcedencia, double pulgadas, boolean sinconizadorTDT) {
        super(referencia, precioConsumo, precioProcedencia);
        this.pulgadas = pulgadas;
        this.sinconizadorTDT = sinconizadorTDT;

    }

    public double getPulgadas() {
        return pulgadas;
    }

    public void setPulgadas(double pulgadas) {
        this.pulgadas = pulgadas;
    }

    public boolean isSinconizadorTDT() {
        return sinconizadorTDT;
    }

    public void setSinconizadorTDT(boolean sinconizadorTDT) {
        this.sinconizadorTDT = sinconizadorTDT;
    }

    public double CalcularTotal() {
        double total = 0;

        if (this.pulgadas > 40) {
            total = (super.getPrecioProcedencia() + super.getPrecioConsumo()) * 1.3;
        } else {
            total = super.getPrecioProcedencia() + super.getPrecioConsumo();
        }
        if (this.sinconizadorTDT) {
            total = total + 250000;
        }
        return total;

    }
}
