package Facturacion.DTO;

public class NeveraDTO extends ElectrodomesticosDTO {
    private int capacidadLitros;

    public NeveraDTO(String referencia, double precioConsumo, double precioProcedencia, int capacidadLitros) {
        super(referencia, precioConsumo, precioProcedencia);
        this.capacidadLitros = capacidadLitros;
    }

    public int getCapacidadLitros() {
        return capacidadLitros;
    }

    public void setCapacidadLitros(int capacidadLitros) {
        this.capacidadLitros = capacidadLitros;
    }

    public double CalcularTotal() {

        if (this.capacidadLitros > 120) {
            return Math.round((super.getPrecioProcedencia() + super.getPrecioConsumo()) *
                    ((Math.round((this.capacidadLitros - 120) / 10) * 0.05) + 1
                    ));
        } else {
            return super.getPrecioProcedencia() + super.getPrecioConsumo();
        }

    }

}
