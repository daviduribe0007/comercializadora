package Facturacion;

import Facturacion.DTO.ElectrodomesticosDTO;
import Facturacion.DTO.NeveraDTO;
import Facturacion.DTO.TvDTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    static String referencia;
    static double tamaño;
    static double consumoPrecio;
    static double procedenciaPrecio;
    static int capacidadEnLitros = 0;
    static double totalFactura = 0;

    static boolean tdt;
    static Scanner scan = new Scanner(System.in);
    static ArrayList<ElectrodomesticosDTO> listaElectrodomesticos = new ArrayList<ElectrodomesticosDTO>();
    static ArrayList<TvDTO> listaTv = new ArrayList<TvDTO>();
    static ArrayList<NeveraDTO> listaNevera = new ArrayList<NeveraDTO>();


    public static void main(String[] args) throws IOException {

        boolean salir = true;
        int producto = 0;

        ElectrodomesticosDTO electrodomestico;
        NeveraDTO nevera;
        TvDTO tv;


        System.out.println("Bien venido a La zona de facturacion!!!");

        do {


            System.out.println("Que producto desea ingresar presione:");
            System.out.println("1. Electrodomestico");
            System.out.println("2. Tv");
            System.out.println("3. Nevera");
            System.out.println("4. Facturar ");
            try {
                producto = Integer.parseInt(scan.nextLine());
            } catch (Exception exception) {
                System.out.println("Solo puede ingresar numeros");
            }

            switch (producto) {
                case 1: {
                    Consultar();
                    electrodomestico = new ElectrodomesticosDTO(referencia, consumoPrecio, procedenciaPrecio);
                    listaElectrodomesticos.add(electrodomestico);

                    break;
                }
                case 2: {
                    boolean tdtBandera = true;
                    int tdtOpcion = 0;
                    Consultar();
                    System.out.println("Ingrese el tamaño del tv en pulgadas");
                    try {
                        tamaño = Double.parseDouble(scan.nextLine());
                    } catch (Exception exception) {
                        System.out.println("Solo puede ingresar numeros");
                    }
                    while (tdtBandera == true) {
                        System.out.println("El tv tiene tdt ?, ingrese:");
                        System.out.println("1. Si");
                        System.out.println("2. No");
                        try {
                            tdtOpcion = Integer.parseInt(scan.nextLine());

                        } catch (Exception exception) {
                            System.out.println("Solo puede ingresar numeros");
                        }


                        switch (tdtOpcion) {
                            case 1:
                                tdt = true;
                                tdtBandera = false;
                                break;
                            case 2:
                                tdt = false;
                                tdtBandera = false;
                                break;
                            default:
                                System.out.println("Valor de procedencia no valido");
                                break;
                        }
                    }

                    tv = new TvDTO(referencia, consumoPrecio, procedenciaPrecio, tamaño, tdt);


                    listaTv.add(tv);

                    break;
                }
                case 3: {
                    Consultar();

                    System.out.println("Ingrese la capacidad en litros exactossin decimales: ");
                    try {
                        capacidadEnLitros = Integer.parseInt(scan.nextLine());
                    } catch (Exception exception) {
                        System.out.println("Solo puede ingresar numeros");
                    }
                    nevera = new NeveraDTO(referencia, consumoPrecio, procedenciaPrecio, capacidadEnLitros);
                    listaNevera.add(nevera);

                    break;
                }
                case 4:
                    salir = false;
                    break;


            }


        } while (salir != false);

        System.out.println("----- Su factura es -----");

        for (int i = 0; i < listaElectrodomesticos.size(); i++) {
            System.out.println(" El producto " + listaElectrodomesticos.get(i).getreferencia() + " Tiene un valor de : " +
                    listaElectrodomesticos.get(i).CalcularTotal());
            totalFactura = totalFactura + listaElectrodomesticos.get(i).CalcularTotal();
        }
        for (int j = 0; j < listaTv.size(); j++) {
            System.out.println(" El tv " + listaTv.get(j).getreferencia() + " Tiene un valor de : " +
                    listaTv.get(j).CalcularTotal());
            totalFactura = totalFactura + listaTv.get(j).CalcularTotal();
        }
        for (int h = 0; h < listaNevera.size(); h++) {
            System.out.println(" La nevera " + listaNevera.get(h).getreferencia() + " Tiene un valor de : " +
                    listaNevera.get(h).CalcularTotal());
            totalFactura = totalFactura + listaNevera.get(h).CalcularTotal();
        }

        System.out.println(" el total de su compra es de: " + totalFactura);

    }

    private static void Consultar() {
        boolean consumo = true, procedencia = true;
        int consumoOpcion = 0, procedenciaOpcion = 0;
        System.out.println("Ingrese la referencia del producto");
        referencia = scan.nextLine();
        while (consumo == true) {
            System.out.println("Ingrese la opcion de consumo:");
            System.out.println("1. Tipo de consumo A");
            System.out.println("2. Tipo de consumo B");
            System.out.println("3. Tipo de consumo C");

            try {
                consumoOpcion = Integer.parseInt(scan.nextLine());
            } catch (Exception exception) {
                System.out.println("Solo puede ingresar numeros");
            }
            switch (consumoOpcion) {
                case 1:
                    consumoPrecio = 450000;
                    consumo = false;
                    break;
                case 2:
                    consumoPrecio = 350000;
                    consumo = false;
                    break;
                case 3:
                    consumoPrecio = 250000;
                    consumo = false;
                    break;
                default:
                    System.out.println("Valor de consumo no valido");
                    break;
            }
        }
        while (procedencia == true) {
            System.out.println("Ingrese la opcion de Procedencia:");
            System.out.println("1. Nacional");
            System.out.println("2. Importado");
            try {
                procedenciaOpcion = Integer.parseInt(scan.nextLine());
            } catch (Exception exception) {
                System.out.println("Solo puede ingresar numeros");
            }
            switch (procedenciaOpcion) {
                case 1:
                    procedenciaPrecio = 250000;
                    procedencia = false;
                    break;
                case 2:
                    procedenciaPrecio = 350000;
                    procedencia = false;
                    break;
                default:
                    System.out.println("Valor de procedencia no valido");
                    break;
            }
        }


    }


}
